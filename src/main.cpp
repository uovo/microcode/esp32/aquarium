#include <OneWire.h>
#include <DallasTemperature.h>
/*#include <WiFi.h>*/
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <PubSubClient.h>
#include <LiquidCrystal_I2C.h>

#define ONE_WIRE_BUS 2
#define LCD_ADDRESS 0x27
#define LCD_COLUMNS 16
#define LCD_LINES 2
#define RELE_01 16

IPAddress ip(192, 168, 50, 19);
IPAddress gateway(192, 168, 50, 1);
IPAddress subnet(255, 255, 255, 0);

const char *BROKER_MQTT = "192.168.50.22";
const char *WIFI_SSID = "SSID";
const char *WIFI_PASSWORD = "PASSWORD";

int BROKER_PORT = 1883;

String HTTP_req;
String URLValue;

void processaPorta(byte porta, byte posicao, WiFiClient cl);
void lePortaDigital(byte porta, byte posicao, WiFiClient cl);
void lePortaAnalogica(byte porta, byte posicao, WiFiClient cl);
String getURLRequest(String *requisicao);
bool mainPageRequest(String *requisicao);

const byte qtdePinosDigitais = 7;
byte pinosDigitais[qtdePinosDigitais] = {2, 4, 5, 12, 13, 14, 15};
byte modoPinos[qtdePinosDigitais] = {INPUT_PULLUP, OUTPUT, OUTPUT, OUTPUT, OUTPUT, OUTPUT, OUTPUT};

const byte qtdePinosAnalogicos = 1;
byte pinosAnalogicos[qtdePinosAnalogicos] = {A0};

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
WiFiClient espClient;
WiFiServer server(80);
// ESP8266WebServer server(80);
PubSubClient MQTTClient(espClient);
LiquidCrystal_I2C lcd(LCD_ADDRESS, LCD_COLUMNS, LCD_LINES);

void MQTTCallback(char *topic, byte *payload, unsigned int length);
void SetupLCD(void);
void SetupMQTT(void);
void SetupWIFI(void);
void SetupWebServer(void);
void CheckConnections(void);
void SetupRelay(void);
void WebServer(void);
void PrintLCD(char *line1, char *line2);

void WebServer()
{
  WiFiClient client = server.available();

  if (client)
  {
    boolean currentLineIsBlank = true;
    while (client.connected())
    {
      if (client.available())
      {
        char c = client.read();
        HTTP_req += c;

        if (c == '\n' && currentLineIsBlank)
        {

          if (mainPageRequest(&HTTP_req))
          {
            URLValue = getURLRequest(&HTTP_req);
            Serial.println(HTTP_req);
            client.println("HTTP/1.1 200 OK");
            client.println("Content-Type: text/html");
            client.println("Connection: keep-alive"); //<------ ATENCAO
            client.println();

            // Conteudo da Página HTML
            client.println("<!DOCTYPE html>");
            client.println("<html>");
            client.println("<head>");
            client.println("<title>m3g4 h0m3</title>");
            client.println("<meta name='viewport' content='width=device-width, initial-scale=1, user-scalable=no'/>");
            client.println("<style>");
            client.println("table,td,th{border:1px solid #000}body{text-align:center;font-family:sans-serif;font-size:25px;padding:25px}p{color:#444}button{outline:0;border:2px solid #1fa3ec;border-radius:18px;background-color:#fff;color:#1fa3ec;padding:5px 25px}button:active{color:#fff;background-color:#1fa3ec}button:hover{border-color:#00f}");
            client.println("</style>");
            client.println("<script>");
            client.println("function LeDadosDoArduino() {");
            client.println("nocache = \"&nocache=\" + Math.random() * 1000000;");
            client.println("var request = new XMLHttpRequest();");
            client.println("var posIni;");
            client.println("var valPosIni;");
            client.println("var valPosFim;");
            client.println("request.onreadystatechange = function() {");
            client.println("if (this.readyState == 4) {");
            client.println("if (this.status == 200) {");
            client.println("if (this.responseText != null) {");

            for (int nL = 0; nL < qtdePinosDigitais; nL++)
            { //<-------NOVO
              client.print("posIni = this.responseText.indexOf(\"PD");
              client.print(pinosDigitais[nL]);
              client.println("\");");
              client.println("if ( posIni > -1) {");
              client.println("valPosIni = this.responseText.indexOf(\"#\", posIni) + 1;");
              client.println("valPosFim = this.responseText.indexOf(\"|\", posIni);");
              client.print("document.getElementById(\"pino");
              client.print(pinosDigitais[nL]);
              client.println("\").checked = Number(this.responseText.substring(valPosIni, valPosFim));");
              client.println("}");
            }

            for (int nL = 0; nL < qtdePinosAnalogicos; nL++)
            { //<-------NOVO

              client.print("posIni = this.responseText.indexOf(\"PA");
              client.print(pinosAnalogicos[nL]);
              client.println("\");");
              client.println("if ( posIni > -1) {");
              client.println("valPosIni = this.responseText.indexOf(\"#\", posIni) + 1;");
              client.println("valPosFim = this.responseText.indexOf(\"|\", posIni);");
              client.print("document.getElementById(\"pino");
              client.print(pinosAnalogicos[nL]);
              client.print("\").innerHTML = \"Porta ");
              client.print(pinosAnalogicos[nL]);
              client.print(" - Valor: \" + this.responseText.substring(valPosIni, valPosFim);");
              client.println("}");
            }

            client.println("}}}}");
            client.println("request.open(\"GET\", \"solicitacao_via_ajax\" + nocache, true);");
            client.println("request.send(null);");
            client.println("setTimeout('LeDadosDoArduino()', 1000);");
            client.println("}");
            client.println("</script>");

            client.println("</head>");

            client.println("<body onload=\"LeDadosDoArduino()\">");
            client.println("<h1>Aqu&aacute;rio</h1>");

            sensors.requestTemperatures();
            float water_temp = sensors.getTempCByIndex(0);
            client.println("<h2>TEMPERATURAS</h2>");
            client.println("&Aacute;gua: ");
            client.print(water_temp);
            client.println("<h2>Dispositivos</h2>");
            client.println("<p>Filtro 01 - ");
            client.println("<a href='?acao=Rele_01On'><button>ON</button></a>");
            client.println("<a href='?acao=Rele_01Off'><button>OFF</button></a></p>");

            client.println("<h1>PORTAS EM FUN&Ccedil;&Atilde;O ANAL&Oacute;GICA</h1>");

            for (int nL = 0; nL < qtdePinosAnalogicos; nL++)
            {

              client.print("<div id=\"pino"); //<----- NOVO
              client.print(pinosAnalogicos[nL]);
              client.print("\">");

              client.print("Porta ");
              client.print(pinosAnalogicos[nL]);
              client.println(" - Valor: ");

              client.print(analogRead(pinosAnalogicos[nL]));
              client.println("</div>"); //<----- NOVO

              client.println("<br/>");
            }

            client.println("<br/>");
            client.println("<h1>PORTAS EM FUN&Ccedil;&Atilde;O DIGITAL</h1>");
            client.println("<form method=\"get\">");

            for (int nL = 0; nL < qtdePinosDigitais; nL++)
            {
              processaPorta(pinosDigitais[nL], nL, client);
              client.println("<br/>");
            }

            client.println("<br/>");
            client.println("<button type=\"submit\">Envia para o ESP8266</button>");
            client.println("</form>");

            client.println("</body>");

            client.println("</html>");
          }
          else if (HTTP_req.indexOf("solicitacao_via_ajax") > -1)
          { //<----- NOVO

            Serial.println(HTTP_req);

            client.println("HTTP/1.1 200 OK");
            client.println("Content-Type: text/html");
            client.println("Connection: keep-alive");
            client.println();

            for (int nL = 0; nL < qtdePinosAnalogicos; nL++)
            {
              lePortaAnalogica(pinosAnalogicos[nL], nL, client);
            }
            for (int nL = 0; nL < qtdePinosDigitais; nL++)
            {
              lePortaDigital(pinosDigitais[nL], nL, client);
            }
          }
          else
          {

            Serial.println(HTTP_req);
            client.println("HTTP/1.1 200 OK");
          }
          HTTP_req = "";
          break;
        }

        if (c == '\n')
        {
          currentLineIsBlank = true;
        }
        else if (c != '\r')
        {
          currentLineIsBlank = false;
        }
      }
    }
    delay(1);
    client.stop();
  }
}
// {
//   String html =
//     "<html>"
//     "<head>"
//     "<meta name='viewport' content='width=device-width, initial-scale=1, user-scalable=no'/>"
//     "<title>Controle Esp8266</title>"
//     "<style>"
//     "body{"
//     "text-align: center;"
//     "font-family: sans-serif;"
//     "font-size:25px;"
//     "padding: 25px;"
//     "}"
//     "p{"
//     "color:#444;"
//     "}"
//     "button{"
//     "outline: none;"
//     "border: 2px solid #1fa3ec;"
//     "border-radius:18px;"
//     "background-color:#FFF;"
//     "color: #1fa3ec;"
//     "padding: 5px 25px;"
//     "}"
//     "button:active{"
//     "color: #fff;"
//     "background-color:#1fa3ec;"
//     "}"
//     "button:hover{"
//     "border-color:#0000ff;"
//     "}"
//     "</style>"
//     "</head>"
//     "<body>"
//     "<p>01 - "
//     "<a href='?acao=Rele_01On'><button>ON</button></a>"
//     "<a href='?acao=Rele_01Off'><button>OFF</button></a></p>"
//     "</body>"
//     "</html>";
//   // server.handleClient();
//   WiFiClient client = server.available();
// }

void processaPorta(byte porta, byte posicao, WiFiClient cl)
{
  static boolean LED_status = 0;
  String cHTML;

  cHTML = "P";
  cHTML += porta;
  cHTML += "=";
  cHTML += porta;

  if (modoPinos[posicao] == OUTPUT)
  {

    if (URLValue.indexOf(cHTML) > -1)
    {
      LED_status = HIGH;
    }
    else
    {
      LED_status = LOW;
    }
    digitalWrite(porta, LED_status);
  }
  else
  {

    LED_status = digitalRead(porta);
  }

  cl.print("<input type=\"checkbox\" name=\"P");
  cl.print(porta);
  cl.print("\" value=\"");
  cl.print(porta);

  cl.print("\"");

  cl.print(" id=\"pino"); //<------NOVO
  cl.print(porta);
  cl.print("\"");

  if (LED_status)
  {
    cl.print(" checked ");
  }

  if (modoPinos[posicao] != OUTPUT)
  {
    cl.print(" disabled ");
  }

  cl.print(">Porta ");
  cl.print(porta);

  cl.println();
}

void lePortaDigital(byte porta, byte posicao, WiFiClient cl)
{
  if (modoPinos[posicao] != OUTPUT)
  {
    cl.print("PD");
    cl.print(porta);
    cl.print("#");

    if (digitalRead(porta))
    {
      cl.print("1");
    }
    else
    {
      cl.print("0");
    }
    cl.println("|");
  }
}

void lePortaAnalogica(byte porta, byte posicao, WiFiClient cl)
{
  cl.print("PA");
  cl.print(porta);
  cl.print("#");

  cl.print(analogRead(porta));

  // especifico para formatar o valor da porta analogica A0
  if (porta == A0)
  {
    cl.print(" (");
    cl.print(map(analogRead(A0), 0, 1023, 0, 179));
    cl.print("&deg;)");
  }

  cl.println("|");
}

String getURLRequest(String *requisicao)
{
  int inicio, fim;
  String retorno;

  inicio = requisicao->indexOf("GET") + 3;
  fim = requisicao->indexOf("HTTP/") - 1;
  retorno = requisicao->substring(inicio, fim);
  retorno.trim();

  return retorno;
}

bool mainPageRequest(String *requisicao)
{
  String valor;
  bool retorno = false;

  valor = getURLRequest(requisicao);
  valor.toLowerCase();

  if (valor == "/")
  {
    retorno = true;
  }

  if (valor.substring(0, 2) == "/?")
  {
    retorno = true;
  }

  if (valor.substring(0, 10) == "/index.htm")
  {
    retorno = true;
  }

  return retorno;
}

void SetupWebServer()
{
  // if (MDNS.begin("esp8266"))
  // {
  //   Serial.println("MDNS responder started");
  // }
  // server.on("/", handleRoot);
  // server.on("/inline", []()
  //           { server.send(200, "text/plain", "this works as well"); });
  // server.onNotFound(handleNotFound);
  // server.begin();
  // Serial.println("HTTP server started");
  server.begin();
}

void SetupRelay()
{
  pinMode(RELE_01, OUTPUT);
  digitalWrite(RELE_01, LOW);
}

void MQTTCallback(char *topic, byte *payload, unsigned int length)
{
  String msg;
  for (int i = 0; i < length; i++)
  {
    char c = (char)payload[i];
    msg += c;
  }

  Serial.print("Received string via MQTT: ");
  Serial.println(msg);
}

void SetupLCD()
{
  delay(10);
  lcd.init();
  lcd.backlight();
  Serial.println("Setting up the LCD");
  lcd.setCursor(0, 0);
  lcd.print("Hello World!");
  delay(5000);
  PrintLCD("Setting up", "LCD");
}

void PrintLCD(char *line1, char *line2)
{
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(line1);
  lcd.setCursor(0, 1);
  lcd.print(line2);
  delay(5000);
}

void SetupMQTT()
{
  delay(10);
  Serial.println("Connecting to m3g4 MQTT..");
  PrintLCD("Connecting to", "m3g4 MQTT");
  MQTTClient.setServer(BROKER_MQTT, BROKER_PORT);
  MQTTClient.setCallback(MQTTCallback);
  delay(5000);
}

void ReconnectMQTT()
{
  while (!MQTTClient.connected())
  {
    Serial.print("Attempting m3g4 MQTT Broker connection...");
    String clientId = "m3g4-esp-client-";
    clientId += String(random(0xffff), HEX);
    if (MQTTClient.connect(clientId.c_str()))
    {
      Serial.println("connected");
      MQTTClient.publish("outTopic", "hello world");
      MQTTClient.subscribe("inTopic");
    }
    else
    {
      Serial.print("failed, rc=");
      Serial.print(MQTTClient.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}

void SetupWIFI()
{
  WiFi.config(ip, gateway, subnet);
  delay(100);
  WiFi.mode(WIFI_STA);
  Serial.println("Connecting to m3g4 network..");
  PrintLCD("Connecting to", "m3g4 network");
  delay(5000);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  delay(5000);
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.println("Connecting to m3g4 network..");
    delay(500);
  }
  Serial.println("Connected to the m3g4 network");
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Connected to");
  lcd.setCursor(0, 1);
  lcd.print("m3g4 network");
  delay(5000);
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void CheckConnections(void)
{
  if (!MQTTClient.connected())
  {
    ReconnectMQTT();
  }
  MQTTClient.loop();

  if (!WiFi.status() == WL_CONNECTED)
  {
    SetupWIFI();
  }
}

void PublishSerial(float water_temp)
{
  Serial.print("Aquario: ");
  Serial.print(water_temp);
  Serial.println(" ºC");
}

void PublishMQTT(float water_temp)
{
  if ((water_temp > -20) && (water_temp < 60))
  {
    MQTTClient.publish("home/lake/temperature", String(water_temp).c_str(), true);
  }
}

void PublishAll()
{

  sensors.requestTemperatures();
  float water_temp = sensors.getTempCByIndex(0);

  PublishMQTT(water_temp);
  PublishSerial(water_temp);
  PrintLCD("Temperature", "Testing");
}

void handleRoot()
{
  String html =
      "<html>"
      "<head>"
      "<meta name='viewport' content='width=device-width, initial-scale=1, user-scalable=no'/>"
      "<title>Controle Esp8266</title>"
      "<style>"
      "body{"
      "text-align: center;"
      "font-family: sans-serif;"
      "font-size:25px;"
      "padding: 25px;"
      "}"
      "p{"
      "color:#444;"
      "}"
      "button{"
      "outline: none;"
      "border: 2px solid #1fa3ec;"
      "border-radius:18px;"
      "background-color:#FFF;"
      "color: #1fa3ec;"
      "padding: 5px 25px;"
      "}"
      "button:active{"
      "color: #fff;"
      "background-color:#1fa3ec;"
      "}"
      "button:hover{"
      "border-color:#0000ff;"
      "}"
      "</style>"
      "</head>"
      "<body>"
      "<p>01 - "
      "<a href='?acao=Rele_01On'><button>ON</button></a>"
      "<a href='?acao=Rele_01Off'><button>OFF</button></a></p>"
      "</body>"
      "</html>";

  // WiFiClient client = server.available();
  // if (!client)
  // {
  //   return;
  // }
  // String req = client.readStringUntil('\r');
  // Serial.println(req);

  // client.print(html);
  // client.flush();
  // if (req.indexOf("acao=Rele_01On") != -1)
  // {
  //   digitalWrite(RELE_01, HIGH);
  // }
  // else if (req.indexOf("acao=Rele_01Off") != -1)
  // {
  //   digitalWrite(RELE_01, LOW);
  // }
  // delay(200);
  // client.stop();

  // server.send(200, "text/html", html);
}

// void handleNotFound()
// {
//   String message = "File Not Found\n\n";
//   message += "URI: ";
//   message += server.uri();
//   message += "\nMethod: ";
//   message += (server.method() == HTTP_GET) ? "GET" : "POST";
//   message += "\nArguments: ";
//   message += server.args();
//   message += "\n";
//   for (uint8_t i = 0; i < server.args(); i++)
//   {
//     message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
//   }
//   server.send(404, "text/plain", message);
// }

void setup()
{
  Serial.begin(9600);
  Serial.begin(115200);
  SetupLCD();
  SetupWIFI();
  SetupMQTT();
  SetupWebServer();
  SetupRelay();
  sensors.begin();
}

void loop()
{
  CheckConnections();
  WebServer();
  PublishAll();
  delay(500);
  MQTTClient.loop();
}